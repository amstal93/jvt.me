{
  "kind" : "bookmarks",
  "slug" : "2020/02/blynj",
  "client_id" : "https://indigenous.realize.be",
  "date" : "2020-02-18T07:59:00Z",
  "h" : "h-entry",
  "properties" : {
    "name" : [ "Rob Landley about the /usr split" ],
    "bookmark-of" : [ "https://blog.w1r3.net/2018/01/06/rob-landley-about-usr-split.html" ],
    "published" : [ "2020-02-18T07:59:00Z" ],
    "category" : [ "linux", "unix" ]
  },
  "tags" : [ "linux", "unix" ]
}
